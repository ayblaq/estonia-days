import { routerMiddleware } from 'connected-react-router'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createRootReducer } from '../reducers';
import { history } from '../routing'

const rootReducer = createRootReducer(history);
const loggerMiddleware = createLogger();
let middleware = applyMiddleware(routerMiddleware(history), thunk, loggerMiddleware);
const store = createStore(rootReducer, compose(middleware));

export { store };