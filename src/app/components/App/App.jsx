import React from 'react'
import { withRouter } from 'react-router'
import { Container } from 'semantic-ui-react'
import './App.scss';

const App = (props) => {

    let {children} = props;

    return (
        <div className="page-layout">
            <main>
                {children}
                <div className="main-content">
                    <Container>
                    </Container>
                </div>
            </main>
        </div>
    )
}

export default withRouter((App))
