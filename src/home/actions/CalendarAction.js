import { fetchDatesApi } from '../api/CalendarApi';
export const SET_START_DATE = 'SET_START_DATE';
export const SET_DATE_VALUES = 'SET_DATE_VALUES';
export const SET_DATE_ERROR = 'SET_DATE_ERROR';

export const setStartDate = (startDate, endDate) => ({
    type: SET_START_DATE,
    payload: {
        startDate: startDate,
        endDate: endDate
    }
})

export const setDateValues = (holidays) => ({
    type: SET_DATE_VALUES,
    payload: {
        holidays:holidays
    }
})

export const setDateError = (message) => ({
    type: SET_DATE_ERROR,
    payload: new Error(message),
    error: true
})

export const fetchDateValues = (data) => (dispatch) => {

    fetchDatesApi(data)
        .then(dateValues => {
            let {holidays} = dateValues
            if(Object.keys(holidays)===0){
                holidays = {"0000-00-00":null}
            }
            dispatch(setDateValues(holidays))
        })
        .catch(error => {
            dispatch(setDateError("Could not get date values."))
        })
}