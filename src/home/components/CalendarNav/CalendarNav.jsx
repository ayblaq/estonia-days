import React, { useState } from 'react'
import { Icon, Button, Dropdown } from 'semantic-ui-react'
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { setStartDate } from '../../actions/CalendarAction'
import './CalendarNav.scss'

const CalendarNav = (props) => {

    const [dates, setDates] = useState(props.initial)
    const dispatch = useDispatch();

    let weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        .reduce(function (result, item, index, array) {
            result[index] = { key: index, value: index, text: item };
            return result;
        }, [])

    let toggleDates = (btnType) => {
        switch (btnType) {
            case "back":
                dates.endDate = moment(dates.startDate).subtract(1, 'days').format('YYYY-MM-DD')
                dates.startDate = moment(dates.endDate).subtract(6, 'days').format('YYYY-MM-DD')
                break;
            default:
                dates.startDate = moment(dates.endDate).add(1, 'days').format('YYYY-MM-DD')
                dates.endDate = moment(dates.startDate).add(6, 'days').format('YYYY-MM-DD')
        }

        setDates(dates)
        dispatch(setStartDate(dates.startDate, dates.endDate))
    }

    let toggleDay = (ev, data) => {
        dates.startDate = moment(dates.startDate).startOf('week').add(data.value, 'days').format('YYYY-MM-DD')
        dates.endDate = moment(dates.startDate).add(6, 'days').format('YYYY-MM-DD')

        setDates(dates)
        dispatch(setStartDate(dates.startDate, dates.endDate))
    }

    return (
        <div className="cal-nav">
            <Button size="mini" icon labelPosition="left" onClick={() => toggleDates("back")}>Prev Week
                <Icon name='left arrow' />
            </Button>
            <Button size="mini" icon labelPosition="right" onClick={() => toggleDates("forward")}>Next Week
                <Icon name='right arrow' />
            </Button>
            <div className="day-nav">
                <span className="desc-span">First day: </span>
                <Dropdown
                    options={weekdays}
                    labeled
                    button
                    onChange={toggleDay}
                    className='icon tiny dropbox'
                    defaultValue={weekdays[0].value}
                />
            </div>
        </div>
    )
};

export default CalendarNav;