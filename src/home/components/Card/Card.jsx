import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './Card.scss';
import { Grid, Popup, Icon } from 'semantic-ui-react'
import { fetchDateValues } from '../../actions/CalendarAction'
import { PageLoader, ErrorPage } from '../../../common/components'
import moment from 'moment';

const Card = () => {

    const {
        calendar: { isFetching, holidays, error, errorMessage, startDate, endDate }
    } = useSelector(state => state.calendar)

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchDateValues({
            startDate: startDate,
            endDate: endDate
        }))
         // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [startDate])

    let daySegment = (day) => {
        return Object.keys(holidays[day]).map((index, k) => {
            return <Popup
                header={`${holidays[day][k].type} holiday`}
                content={`${holidays[day][k].name}`}
                position='top center'
                key={k}
                trigger={
                    <p className={`${holidays[day][k].type}`}>
                        <Icon circular name={holidays[day][k].type.toLowerCase() === "folk" ? "talk" : "announcement"} />
                        {holidays[day][k].name}
                    </p>
                }
            />
        })
    }

    if (isFetching) return <PageLoader />;
    if (error) return <ErrorPage content={errorMessage} title="OOPS!!! Technical Error" />;

    return (
            <Grid stackable columns={7} className="calendar-container" padded>
                <Grid.Row textAlign='center' className="calendar-card">
                    {[...Array(7)].map((key, i) => {
                        let date = moment(startDate).add(i, 'days')
                        let day = date.format('YYYY-MM-DD')
                        return <Grid.Column key={i} className="date-card">
                            <div className="date-header">
                                <div className="day">
                                    {date.format('dddd')}
                                </div>
                                <div className="date">
                                    {day}
                                </div>
                            </div>
                            <div className="date-content">
                                {holidays[day] !== undefined ? daySegment(day) : ''}
                            </div>
                        </Grid.Column>
                    })}
                </Grid.Row>
            </Grid>
    )
}

export default Card;