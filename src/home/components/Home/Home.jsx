import React from 'react'
import { Container } from 'semantic-ui-react'
import './Home.css';
import CalendarNav from '../CalendarNav/CalendarNav';
import Card from '../Card/Card';
import moment from 'moment';

const Home = () => {

    const dates = {
        endDate: moment().endOf('week').format('YYYY-MM-DD'),
        startDate: moment().startOf('week').format('YYYY-MM-DD')
    }

    return (
        <Container fluid>
            <div className="App-header">
                <CalendarNav initial={dates}></CalendarNav>
                <div className="app-container">
                    <Card></Card>
                </div>
            </div>
        </Container>
    )
};

export default Home;






