import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { calendarReducer } from './CalendarReducer'

const persistConfig = {
  key: 'calendar',
  storage: storage,
};

const calendarReducers = (state = {}, action) => {
  return {
    calendar: calendarReducer(state.calendar, action),
  }
};

const reducer = persistReducer(persistConfig, calendarReducers);

export { reducer as calendarReducers }
