import {
    SET_DATE_ERROR,
    SET_DATE_VALUES,
    SET_START_DATE
} from '../actions/CalendarAction'
import moment from 'moment'


export const calendarReducer = (state = {
    isFetching: true,
    startDate: moment().startOf('week').format('YYYY-MM-DD'),
    endDate: moment().endOf('week').format('YYYY-MM-DD'),
    holidays: {},
    errorMessage: '',
    error: false
}, action) => {
    switch (action.type) {
        case SET_START_DATE:
            return {
                ...state,
                isFetching: true,
                error: false,
                startDate: action.payload.startDate,
                endDate: action.payload.endDate,
            };
        case SET_DATE_VALUES:
            return {
                ...state,
                isFetching: false,
                errorMessage: '',
                error: false,
                holidays: action.payload.holidays
            };
        case SET_DATE_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.payload.message,
                error: true
            }
        default:
            return state
    }
}