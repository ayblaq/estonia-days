import { config } from '../../config';
import axios from 'axios';

export async function fetchDatesApi(data) {

    return await axios({
        method: "post",
        url: `${config.apiUrl}/api/holidays`,
        data: { ...data, apiKey: config.apiKey }
    }).then((response) => {
        return response.data
    }).catch(error => { throw error })
}