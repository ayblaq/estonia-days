import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { calendarReducers } from '../home/reducers'

export const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  calendar: calendarReducers
});
