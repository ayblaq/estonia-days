// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import * as serviceWorker from './serviceWorker';

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import { Root } from './app/components'
import { Routing, history} from './routing'
import { store } from './store'
import './styles/index.scss'
import 'semantic-ui-css/semantic.min.css';

if (process.env.NODE_ENV === 'development') {
    console.log("This is development")
}

render(
    <Provider store={store}>
        <Root routes={Routing} history={history} store={store} rKey={Math.random()}/>
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();