import React from 'react'
import { Loader } from 'semantic-ui-react'
import './PageLoader.scss'

const PageLoader = () => {

    return (
        <Loader active inline='centered' size='small'>
            Fetching dates.
        </Loader>
    )
}

export default PageLoader;
