This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and [semantic ui react](https://react.semantic-ui.com/)

## About 

A simple one week calendar based on Estonia public and folk holidays. 

## Demo

Check [demo](https://ayblaq.gitlab.io/estonia-days)

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.




